package com.example.cartaslluis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DificultyActivity extends AppCompatActivity {

    protected Button ezBtn;
    protected Button nrmlBtn;
    protected Button hardBtn;


    protected Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dificulty);

        intent = new Intent(this, MainActivity.class);

        ezBtn = (Button) findViewById(R.id.ezBtn);
        ezBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                System.out.println("[INFO] Cargando EZ mode");
                intent.putExtra("difficulty", "EZ");
                startActivity(intent);
            }

        });

        nrmlBtn = (Button) findViewById(R.id.nrmlBtn);
        nrmlBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                System.out.println("[INFO] Cargando NORMAL mode");
                intent.putExtra("difficulty", "NORMAL");
                startActivity(intent);
            }

        });

        hardBtn = (Button) findViewById(R.id.hardBtn);
        hardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                System.out.println("[INFO] Cargando HARD mode");
                intent.putExtra("difficulty", "HARD");
                startActivity(intent);
            }

        });
    }
}




