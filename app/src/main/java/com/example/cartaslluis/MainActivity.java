package com.example.cartaslluis;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;


    private List<Carta> cardList = new ArrayList<>();
    private List<Integer> frontList = new ArrayList<>();

    int backCardId = R.drawable.back;

    protected String dificultad;
    protected int columns = 3;
    protected int maxRange;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();

        dificultad = bundle.getString("difficulty");

        switch(dificultad) {
            case("EZ"):
            {
                maxRange = 3; //number of pairs
                break;
            }
            case("NORMAL"):
            {
                maxRange = 6;
                break;
            }
            case("HARD"):
            {
                maxRange = 8;
                columns = 4;
                break;
            }
        }

        TextView timerText = findViewById(R.id.timerText);


        //Donde guardamos las referencias a las imagene
        frontList.add(R.drawable.c0);
        frontList.add(R.drawable.c1);
        frontList.add(R.drawable.c2);
        frontList.add(R.drawable.c3);
        frontList.add(R.drawable.c4);
        frontList.add(R.drawable.c5);
        frontList.add(R.drawable.c6);
        frontList.add(R.drawable.c7);
        frontList.add(R.drawable.c8);
        frontList.add(R.drawable.c9);
        frontList.add(R.drawable.c10);
        frontList.add(R.drawable.c11);

        //0,3 ;
        for (int i = 0; i < maxRange; i++) {
            cardList.add(new Carta(frontList.get(i), backCardId));
            cardList.add(new Carta(frontList.get(i), backCardId));
        }
        /*
        //Coge los dobles
        for (int i = 6; i < frontList.size(); i++) {
            cardList.add(new Carta(frontList.get(i), backCardId));
            cardList.add(new Carta(frontList.get(i), backCardId));
        }*/

        //Desordena de la lista de referencias a las imagenes
        Collections.shuffle(cardList);

        //Get the recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        //No need for scroll, fixed is good enough
        recycler.setHasFixedSize(true);

        lManager = new GridLayoutManager( this, columns);
        recycler.setLayoutManager(lManager);

        adapter = new CardAdapter(cardList, this, timerText);
        recycler.setAdapter(adapter);
    }
}
