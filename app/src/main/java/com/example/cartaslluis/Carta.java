package com.example.cartaslluis;

public class Carta {

    private int front;
    private int back;
    private State state;


    public enum State {
        FRONT, BACK, FIXED
    }

    public Carta (int front, int back) {
        this.front = front;
        this.back = back;
        this.state = State.BACK;
    }

    public int getImagen() {

        if (this.state == State.BACK) {
            return this.back;
        } else {
            return this.front;
        }
    }

    public int getId() {
        return this.back;
    }

    public State getState() {
        return this.state;
    }

    public void girarCarta(){

        //if stat == BACK -> girar and thisState -> FRONT; else -> BACK
        if (getState() == State.BACK) {
            this.state = State.FRONT;
        } else {
            this.state = State.BACK;
        }
    }

    public int getFrontImage() {
        return this.front;
    }

    public void setState(State state) {
        this.state = state;
    }
}
