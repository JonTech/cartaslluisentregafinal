package com.example.cartaslluis;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CartaViewHolder> {

    private List<Carta> cartas;
    private Context context;

    private TextView timerText;


    Carta carta;
    Carta otraCarta;

    CountDownTimer timer;
    CountDownTimer nonEqualTimer;

    int timerTick = 30;
    int puntos = 0;

    boolean iniciar = true;
    boolean jugando = true;

    boolean canChoose = true;


    //Constructor
    public CardAdapter(List<Carta> cartas, Context context, TextView timerText) {
        this.cartas = cartas;
        this.context = context;
        this.timerText = timerText;
    }


    public class CartaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imagen;



        public CartaViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            imagen.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            //When you click on a card first it's checked if the game is initialized
            if(iniciar) {

                //Se inica un contador de 30 secs que decrece segundo a segundo
                timer = new CountDownTimer(30000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                        timerTick--; //Descontamos tick a tick;
                        timerText.setText(String.valueOf(timerTick));
                        iniciar = false;
                    }

                    @Override
                    public void onFinish() {

                        Toast.makeText(context, "GAME OVER!", Toast.LENGTH_SHORT).show();
                        jugando = false; //Ya no se puede seguir jugando
                    }
                }.start(); //Comenzar el timer
            }//END iniciar

            /*Ahora comenzamos con el algoritmo de juego, por defecto cuando se inicia el 'timer'
              arriba, tambien se puede jugar*/

            if (jugando) {

                //Haciendo click sobre la carta seleccionamos la que queremos y la guardamos en el obj
                //del tipo carta
                if (canChoose) {
                    carta = cartas.get(getAdapterPosition());
                    System.out.println("[SYSTEM INFO] CAN choose");
                } else {
                    System.out.println("[SYSTEM INFO] Can't choose");
                }


                switch(carta.getState()) {

                    //Si esta con el reverso
                    case BACK:
                    {
                        //Y no hay otra carta seleccionada
                        if (otraCarta == null) {

                            //Guarda la carta previamente seleecionada si no ha sido escogida
                            otraCarta = carta;
                            otraCarta.girarCarta();
                            notifyDataSetChanged();
                            canChoose = true;

                        } else {

                            canChoose = false;
                            //Girame la carta de ahora (la segunda seleccionada)
                            carta.girarCarta();
                            notifyDataSetChanged();

                            //Si las dos son iguales
                            if (otraCarta.getFrontImage() == carta.getFrontImage()) {


                                carta = null;
                                otraCarta = null;
                                puntos++;
                                System.out.println("[INFO] Cards are equal! Adding points: " + puntos);
                                canChoose = true;


                                //Si se consiguen los 6 puntos
                                if (puntos == (cartas.size() / 2)) {

                                    Toast.makeText(context, "You Won!", Toast.LENGTH_SHORT);
                                    timerText.setText("You Won!");
                                    timer.cancel();
                                    jugando = false; //Block game
                                }
                            }
                            //Las cartas no son iguales
                            else {

                                //Un nuevo timer porque sino... estariamos restando en -1 segundo al timer de antes haciendo que se vuelva loco
                                nonEqualTimer = new CountDownTimer(1000, 1000) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        canChoose = false; //Paramos escoger durante 1 segundo
                                    }

                                    @Override
                                    public void onFinish() {
                                        //Las volvemos a girar
                                        carta.girarCarta();
                                        otraCarta.girarCarta();

                                        //Nullificamos
                                        carta = null;
                                        otraCarta = null;

                                        jugando = true;
                                        canChoose = true;
                                        notifyDataSetChanged();
                                    }
                                }.start();
                            }
                        }
                        break;
                    }
                }
            }
        }
    }


    @Override
    public int getItemCount() { return cartas.size(); }

    @Override
    public CartaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_table, viewGroup, false);
        return new CartaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CardAdapter.CartaViewHolder cartaViewHolder, int i) {
        cartaViewHolder.imagen.setImageResource(cartas.get(i).getImagen());
    }

}
